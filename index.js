class Axessibility {
  constructor(settings) {
    this.images = true;
    this.fontSize = 16;
    this.contrast = 'normal';
    this.wrapper = undefined;
    this.readingLine = false;
    this.largeCursor = false;
    this.filters = {
      grayscale: false,
      inverted: false,
      sepia: false,
    };
    this.icons = {
      check: '<svg class="axessibilityIMG" xmlns="http://www.w3.org/2000/svg" width="25" height="22"><path d="M21.397.304l3.206 2.392-14.101 18.888-9.898-9.652 2.792-2.864 6.63 6.466z" fill="currentColor" fill-rule="nonzero"/></svg>',
      uncheck: '<svg class="axessibilityIMG" xmlns="http://www.w3.org/2000/svg" width="20" height="20"><path d="M16.915.128l2.854 2.803-6.972 7.098 6.972 7.1-2.854 2.802-6.92-7.048-6.92 7.048L.22 17.128l6.971-7.099-6.97-7.098L3.074.128l6.92 7.047L16.915.128z" fill="currentColor" fill-rule="nonzero"/></svg>',
    }

    this.settings = {
      background: '#253786',
      color: '#fff',
      fontSize: true,
      contrast: true,
      images: true,
      largeCursor: true,
      readingLine: true,
      filters: ['grayscale', 'invert', 'sepia'],
      position: 'bottom-left',
      fontSizeStep: 2,
      fontSizeMaxSteps: 4,
    }

    this.settings = Object.assign(this.settings, settings)
    this.init();
  }

  init() {
    this.clearAll('.axessibility');
    this.clearAll('.axessibility__Toggle');
    this.clearAll('.axessibility__ReadingLine');
    this.clearAll('.axessibility__LargeCursor');
    this.changeStyleSheet(this.convertToREM)
    this.setStyles()
    this.wrapper = document.createElement('DIV');
    this.wrapper.classList.add('axessibility');
    this.wrapper.classList.add(this.settings.position);

    let title = document.createElement('H4');
    title.innerHTML = 'Settings'

    let closeButton = this.createControlButton('axessibility__Close', '<svg class="axessibilityIMG" xmlns="http://www.w3.org/2000/svg" width="20" height="20"><path d="M16.695 0l2.854 2.803-6.972 7.098L19.549 17l-2.854 2.803-6.921-7.048-6.92 7.048L0 17l6.971-7.099L0 2.803 2.854 0l6.92 7.047L16.694 0z" fill="currentColor" fill-rule="nonzero"/></svg>', 'Close accessibility panel')
    closeButton.addEventListener('click', () => this.close())

    title.appendChild(closeButton);
    this.wrapper.appendChild(title)

    // FONT
    if (this.settings.fontSize) {
      this.createSingleWrapper('Font size', {
        method: this.changeFontSize,
        buttons: [{
          innerHTML: '<svg class="axessibilityIMG" xmlns="http://www.w3.org/2000/svg" width="11" height="12"><path d="M2.61 11.5l.827-2.719h4.165L8.43 11.5h2.61L6.992.031h-2.96L0 11.5h2.61zm4.413-4.75H4.047c.812-2.63 1.305-4.279 1.476-4.945.042.187.111.442.207.765.097.323.528 1.716 1.293 4.18z" fill="currentColor" fill-rule="nonzero"/></svg>',
          props: '-',
          ariaLabel: 'Decrease font size'
        },
        {
          innerHTML: '<svg class="axessibilityIMG" width="25" height="26" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M5.91 26l1.873-6.164h9.432L19.09 26H25L15.833 0H9.13L0 26h5.91zm9.993-10.768H9.165c1.84-5.962 2.956-9.7 3.343-11.21.095.424.251 1.002.469 1.734.22.732 1.195 3.89 2.927 9.476z" fill="currentColor"/></svg>',
          props: '+',
          ariaLabel: 'Increase font size'
        }]
      });
    }

    // CONTRAST
    if (this.settings.contrast) {
      this.createSingleWrapper('Contrast', {
        method: this.changeContrast,
        toggleState: true,
        buttons: [{
          innerHTML: '<svg class="axessibilityIMG" xmlns="http://www.w3.org/2000/svg" width="26" height="27"><path d="M13.092.5c.073 0 .146 0 .218.002C10.694 2.287 8.978 5.28 8.978 8.67c0 5.471 4.467 9.906 9.976 9.906A9.982 9.982 0 0026 15.682C24.956 21.822 19.574 26.5 13.092 26.5 5.862 26.5 0 20.68 0 13.5S5.862.5 13.092.5z" fill="currentColor" fill-rule="evenodd"/></svg>',
          props: 'dark',
          ariaLabel: 'Toggle dark mode'
        },
        {
          innerHTML: '<svg class="axessibilityIMG" xmlns="http://www.w3.org/2000/svg" width="29" height="29"><path d="M14.5 24.376c.47 0 .85.38.85.85v2.924a.85.85 0 11-1.7 0v-2.925c0-.469.38-.85.85-.85zm3.769-.748a.851.851 0 011.11.461l1.115 2.704a.85.85 0 11-1.57.649l-1.117-2.704a.85.85 0 01.462-1.11zm-8.67.45a.85.85 0 011.57.652l-1.123 2.701a.85.85 0 01-1.569-.651zm11.884-2.595a.85.85 0 011.202 0l2.068 2.069a.849.849 0 11-1.201 1.201l-2.069-2.068a.85.85 0 010-1.202zm-15.168 0a.85.85 0 111.202 1.202l-2.069 2.068a.846.846 0 01-1.201 0 .849.849 0 010-1.201zM14.5 6.627c4.341 0 7.873 3.532 7.873 7.873s-3.532 7.873-7.873 7.873-7.873-3.532-7.873-7.873 3.532-7.873 7.873-7.873zm9.12 11.663a.85.85 0 011.11-.459l2.701 1.123a.849.849 0 11-.652 1.569l-2.701-1.122a.85.85 0 01-.458-1.111zm-19.358-.483a.85.85 0 01.648 1.572l-2.703 1.115a.85.85 0 01-.649-1.571zM28.15 13.65a.85.85 0 110 1.7h-2.925a.85.85 0 110-1.7zm-24.375 0a.85.85 0 110 1.7H.85a.85.85 0 110-1.7zm23.018-5.144a.85.85 0 11.649 1.57l-2.704 1.117a.85.85 0 01-.648-1.572zm-25.683.43a.85.85 0 011.11-.46l2.702 1.123c.433.18.639.678.458 1.11a.847.847 0 01-1.11.46l-2.701-1.123a.849.849 0 01-.459-1.11zm22.442-4.69a.85.85 0 011.201 1.202l-2.068 2.069a.848.848 0 01-1.201-.001.846.846 0 010-1.2zm-19.305 0a.852.852 0 011.201 0l2.069 2.07a.847.847 0 010 1.2.845.845 0 01-1.201 0l-2.07-2.068a.852.852 0 010-1.201zm14.707-2.677a.849.849 0 111.57.651l-1.123 2.702a.85.85 0 01-1.57-.652zm-9.987-.472a.851.851 0 011.11.461l1.116 2.704a.85.85 0 01-1.571.649L8.506 2.207a.85.85 0 01.46-1.11zM14.5 0c.47 0 .85.38.85.85v2.925a.85.85 0 11-1.7 0V.85c0-.47.38-.85.85-.85z" fill="currentColor" fill-rule="nonzero"/></svg>',
          props: 'light',
          ariaLabel: 'Toggle light mode'
        }]
      });
    }

    // IMAGES
    if (this.settings.images) {
      this.createSingleWrapper('Images', {
        method: this.changeImagesVisibility,
        changeState: true,
        buttons: [{
          innerHTML: this.icons.check,
          props: true,
          default: true,
          ariaLabel: 'Turn on images'
        },
        {
          innerHTML: this.icons.uncheck,
          props: false,
          ariaLabel: 'Turn off images'
        }]
      });
    }

    // READING LINE
    if (this.settings.readingLine) {
      this.createSingleWrapper('Reading line', {
        method: this.changeReadingLineState,
        changeState: true,
        buttons: [{
          innerHTML: this.icons.check,
          props: true,
          ariaLabel: 'Turn on reading line'
        },
        {
          innerHTML: this.icons.uncheck,
          props: false,
          default: true,
          ariaLabel: 'Turn off reading line'
        }]
      });
    }
    
    // LARGE CURSOR
    if (this.settings.largeCursor) {
      this.createSingleWrapper('Large cursor', {
        method: this.changeLargeCursorState,
        changeState: true,
        buttons: [{
          innerHTML: '<svg class="axessibilityIMG" xmlns="http://www.w3.org/2000/svg" width="25" height="22"><path d="M21.397.304l3.206 2.392-14.101 18.888-9.898-9.652 2.792-2.864 6.63 6.466z" fill="currentColor" fill-rule="nonzero"/></svg>',
          props: true,
          ariaLabel: 'Turn on reading line'
        },
        {
          innerHTML: '<svg class="axessibilityIMG" xmlns="http://www.w3.org/2000/svg" width="20" height="20"><path d="M16.915.128l2.854 2.803-6.972 7.098 6.972 7.1-2.854 2.802-6.92-7.048-6.92 7.048L.22 17.128l6.971-7.099-6.97-7.098L3.074.128l6.92 7.047L16.915.128z" fill="currentColor" fill-rule="nonzero"/></svg>',
          props: false,
          default: true,
          ariaLabel: 'Turn off reading line'
        }]
      });
    }

    // FILTERS
    if (this.settings.filters) {
      this.createSingleWrapper('Filters', {
        method: this.changeFilter,
        multipleState: true,
        buttons: this.settings.filters.map(f => ({
          innerHTML: f[0].toUpperCase(),
          props: f,
          ariaLabel: `Toggle ${f} filter`
        }))
      });
    }

    this.wrapper.addEventListener('click', e => {
      e.stopPropagation()
    });

    document.body.appendChild(this.wrapper)

    let toggle = this.createControlButton('axessibility__Toggle', '<svg class="axessibilityIMG" xmlns="http://www.w3.org/2000/svg" width="29" height="32"><path d="M26.496 8.81a1.785 1.785 0 010 3.568h-7.32v16.946a1.784 1.784 0 01-3.568 0v-7.58h-2.676v7.58a1.783 1.783 0 11-3.567 0V12.378H1.784a1.783 1.783 0 110-3.567h24.712zM13.824 0a3.568 3.568 0 013.568 3.568v.446a3.567 3.567 0 01-7.135 0v-.446A3.568 3.568 0 0113.824 0z" fill="currentColor" fill-rule="evenodd"/></svg>', 'Open accessibility panel')
    toggle.classList.add(this.settings.position)

    toggle.addEventListener('click', (e) => {
      e.stopPropagation()
      this.isActive = !this.isActive;
      this.wrapper.classList[this.isActive ? 'add' : 'remove']('isActive')
    })

    document.body.appendChild(toggle)

    document.body.addEventListener('click', () => this.close())
  }

  createControlButton(className, innerHTML, ariaLabel) {
    let el = document.createElement('BUTTON');
    el.classList.add(className);
    el.innerHTML = innerHTML
    el.setAttribute('aria-label', ariaLabel)
    return el;
  }

  close() {
    this.isActive = false;
    this.wrapper.classList.remove('isActive')
  }

  clearAll(selector) {
    if (document.querySelector(selector)) {
      for (const item of document.querySelectorAll(selector)) {
        document.body.removeChild(item)
      }
    }
  }

  createSingleWrapper(text, settings) {
    let wrapper = document.createElement('DIV');
    wrapper.classList.add('axessibility__Item')

    let innerWrapper = document.createElement('DIV');
    let textElement = document.createElement('H3');
    textElement.innerHTML = text;

    for (const button of settings.buttons) {
      let btn = document.createElement('BUTTON')
      btn.innerHTML = button.innerHTML;
      btn.setAttribute('aria-label', button.ariaLabel)
      if (button.default) btn.classList.add('isActive');
      btn.addEventListener('click', e => {
        settings.method(button.props, this, e)
        if ((settings.changeState || settings.toggleState) && !settings.multipleState) {
          for (const b of [...btn.parentNode.querySelectorAll('button')]) {
            if (b !== btn) b.classList.remove('isActive');
          }
          if (settings.toggleState)
            btn.classList[btn.classList.contains('isActive') ? 'remove' : 'add']('isActive');
          else btn.classList.add('isActive')
        } else if (settings.multipleState) {
          btn.classList.toggle('isActive')
        }
      })
      innerWrapper.appendChild(btn)
    }

    wrapper.appendChild(textElement)
    wrapper.appendChild(innerWrapper)

    this.wrapper.appendChild(wrapper);
  }

  changeStyleSheetHelper(sheet, func, options) {
    try {
      if (sheet) {
        for (const rule of sheet.cssRules) {
          if ([CSSRule.MEDIA_RULE, CSSRule.STYLE_RULE].includes(rule.type)) {
            if (rule.media && rule.media.length > 0) {
              this.changeStyleSheetHelper(rule, func, options)
            } else {
              func(rule, options)
            }
          }
        }
      }
    } catch (e) {

    }
  }

  changeStyleSheet(func, options) {
    for (const sheet of document.styleSheets) {
      this.changeStyleSheetHelper(sheet, func, options)
    }
  }

  convertToREM(rule) {
    const {
      fontSize
    } = rule.style;

    const [all, number, unit] = fontSize.match(/^([\d.]+)(\D+)$/) || [];
    if (unit === "px") {
      rule.style.fontSize = `${(number / 16)}rem`
    }
  }


  convertColor(rule, color) {
    if (!rule.selectorText.includes('axessibility')) {
      if (!rule.style.hasOwnProperty('oldColor')) {
        rule.style.oldColor = rule.style.color;
        rule.style.oldBackgroundColor = rule.style.backgroundColor;
      }

      if (color !== 'normal') {
        let backgroundColor = color === 'light' ? 'white' : 'black';
        if (!['', 'transparent'].includes(rule.style.color)) rule.style.color = color === 'light' ? 'black' : 'white';
        if (!['', 'transparent'].includes(rule.style.backgroundColor)) rule.style.backgroundColor = backgroundColor;
      } else {
        rule.style.color = rule.style.oldColor;
        rule.style.backgroundColor = rule.style.oldBackgroundColor;
      }
    }
  }

  changeFilter(type, self) {
    let filter = getComputedStyle(document.documentElement, null).getPropertyValue('filter');
    filter = filter === 'none' ? '' : filter;
    filter = filter.replace(/\(100%\)/g, '1')

    self.filters[type] = !self.filters[type]

    for (const key in self.filters) {
      if (key === type) {
        if (filter.includes(key)) {
          filter = filter.replace(new RegExp(`${key}\\(.\\)`, 'g'), '')
        } else filter += ` ${key}(1) `
      }
    }

    if (filter === '') filter = 'none';
    filter = filter.replace(/inverted/g, 'invert')
    document.documentElement.style.filter = filter;

    self.wrapper.style.filter = filter.replace(/invert\\(1\\)/g, 'invert(0)')
  }

  changeImages() {
    if (!this.images) {
      if (!document.querySelector('#axessibilitySheet')) {
        let style = document.createElement("style");
        style.appendChild(document.createTextNode(""));
        style.id = 'axessibilitySheet'
        document.head.appendChild(style);
        this.addCSSRule(style, 'html body img:not(.axessibilityIMG), html body picture, html body svg:not(.axessibilityIMG)', 'visibility: hidden')
        this.changeStyleSheet(this.addOrRemoveBackground, true)
      }
    } else {
      document.querySelector('#axessibilitySheet').remove()
      this.changeStyleSheet(this.addOrRemoveBackground, false)
    }
  }

  addOrRemoveBackground(rule, removeBackground) {
    if (removeBackground) {
      rule.style.oldBackgroundImage = rule.style.backgroundImage;
      rule.style.backgroundImage = 'none !important';
    } else {
      rule.style.backgroundImage = `${rule.style.oldBackgroundImage} !important`;
    }
  }

  addCSSRule({ sheet }, selector, rule) {
    if ("insertRule" in sheet) {
      sheet.insertRule(`${selector}{${rule} !important}`);
    } else if ("addRule" in sheet) {
      sheet.addRule(selector, `${rule} !important`);
    }
  }

  changeFontSize(type, self) {
    let maxStep = self.settings.fontSizeMaxSteps * self.settings.fontSizeStep;

    if ((type === '+' && self.fontSize < maxStep + 16) || (type === '-' && self.fontSize > 16 - maxStep)) {
      if (type === '-') self.fontSize -= self.settings.fontSizeStep;
      else self.fontSize += self.settings.fontSizeStep;

      document.documentElement.style.fontSize = `${self.fontSize}px`
    }

  }

  changeContrast(type, self) {
    if (type === self.contrast) type = 'normal'
    self.contrast = type;
    self.changeStyleSheet(self.convertColor, type);
    if (type !== 'normal') {
      if (document.documentElement.getAttribute('data-axessibility-color') === null) {
        document.documentElement.setAttribute('data-axessibility-color', document.documentElement.style.color)
      }
      if (document.documentElement.getAttribute('data-axessibility-background') === null) {
        document.documentElement.setAttribute('data-axessibility-background', document.documentElement.style.background)
      }
      document.documentElement.style.color = type === 'dark' ? 'white' : 'black';
      document.documentElement.style.background = type === 'dark' ? 'black' : 'white';
    } else {
      document.documentElement.style.removeProperty('color');
      document.documentElement.style.removeProperty('background');
      document.documentElement.style.color = document.documentElement.dataset.axessibilityColor;
      document.documentElement.style.background = document.documentElement.dataset.axessibilityBackground;
      document.documentElement.removeAttribute('data-axessibility-color')
      document.documentElement.removeAttribute('data-axessibility-background')
    }
  }

  changeImagesVisibility(visible, self) {
    self.images = visible;
    self.changeImages()
  }

  changeReadingLineState(isVisible, self, e) {
    if (isVisible && !self.readingLine) {
      let el = document.createElement('DIV')
      el.classList.add('axessibility__ReadingLine')
      document.body.addEventListener('mousemove', e => {
        el.style.transform = `translateY(${e.clientY}px)`
      })
      el.style.transform = `translateY(${e.clientY}px)`
      document.body.appendChild(el)
    }
    
    if(!isVisible && self.readingLine){
      document.body.removeChild(document.querySelector('.axessibility__ReadingLine'))
    }

    self.readingLine = isVisible;
  }

  changeLargeCursorState(isVisible, self, e) {
    if (isVisible && !self.largeCursor) {
      let el = document.createElement('div')
      el.innerHTML = '<svg class="axessibilityIMG" width="113" height="184" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.772 3.236A2.5 2.5 0 002.5 5v143.954a2.5 2.5 0 004.014 1.989l31.108-23.679 26.646 52.816a2.5 2.5 0 003.35 1.11l28-14a2.5 2.5 0 001.11-3.37l-27.151-53.366H107.5a2.501 2.501 0 001.772-4.264L6.772 3.236z" fill="#fff" stroke="#000" stroke-width="5" stroke-linejoin="round"/></svg>'
      el.classList.add('axessibility__LargeCursor')
      document.body.addEventListener('mousemove', e => {
        el.style.transform = `translate3d(${e.clientX}px, ${e.clientY}px, 0)`
      })
      el.style.transform = `translate3d(${e.clientX}px, ${e.clientY}px, 0)`
      document.body.appendChild(el)
    }
    
    if(!isVisible && self.largeCursor) {
      document.body.removeChild(document.querySelector('.axessibility__LargeCursor'))
    }

    self.largeCursor = isVisible;

    self.changeStyleSheet(self.changeCursorStyle, isVisible)
  }

  changeCursorStyle(rule, isVisible){
    if (!rule.style.hasOwnProperty('oldCursor')) {
      rule.style.oldCursor = rule.style.cursor;
    }

    rule.style.cursor = isVisible ? 'none' : rule.style.oldCursor
  }

  setStyles() {
    let style = document.createElement("style");
    style.appendChild(document.createTextNode(""));
    document.head.appendChild(style);
    style.innerHTML = `
    .axessibility__Toggle{
      position: fixed;
      width: 52px;
      height: 52px;
      border-radius: 50%;
      z-index: 1000000;
      border: 0;
      outline: 0;
      color: ${this.settings.color};
      cursor: pointer;
      transition: box-shadow 300ms;
      background-color: ${this.settings.background};
    }
    
    .axessibility__Toggle:hover{
      box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.12)
    }

    .axessibility__Toggle svg{
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    }
    
    .axessibility{
      position: fixed;
      z-index: 10000000;
      background-color: ${this.settings.background};
      padding: 33px 24px 33px 37px;
      opacity: 0;
      max-width: 440px;
      width: 100%;
      max-height: 100%;
      overflow: auto;
      visibility: hidden;
      transition: opacity 300ms, visibility 300ms;
    }
    
    .axessibility.top-left{
      border-top-left-radius: 26px;
    }
    
    .axessibility.top-right{
      border-top-right-radius: 26px;
    }
    
    .axessibility.bottom-left{
      border-bottom-left-radius: 26px;
    }
    
    .axessibility.bottom-right{
      border-bottom-right-radius: 26px;
    }
    
    .axessibility.top-left,
    .axessibility.top-right,
    .axessibility__Toggle.top-left,
    .axessibility__Toggle.top-right{
      top: 20px;
      bottom: auto;
    }
    
    .axessibility.top-right,
    .axessibility__Toggle.top-right{
      right: 20px;
    }
    
    .axessibility.top-left,
    .axessibility__Toggle.top-left{
      left: 20px;
    }
    
    .axessibility.bottom-left,
    .axessibility.bottom-right,
    .axessibility__Toggle.bottom-left,
    .axessibility__Toggle.bottom-right{
      top: auto;
      bottom: 20px;
    }
    
    .axessibility.bottom-right,
    .axessibility__Toggle.bottom-right{
      right: 20px;
    }
    
    .axessibility.bottom-left,
    .axessibility__Toggle.bottom-left{
      left: 20px;
    }
    
    .axessibility.isActive{
      opacity: 1;
      visibility: visible;
    }
    
    .axessibility h4{
      display: flex;
      align-items: flex-start;
      justify-content: space-between;
      font-size: 23px;
      line-height: 1.09;
      letter-spacing: -0.58px;
      color: ${this.settings.color};
      margin: 0 0 20px;
      padding-bottom: 25px;
      border-bottom: 1px solid ${this.settings.color};
    }
    
    .axessibility__Close{
      background-color: transparent;
      border: 0;
      outline: 0;
      padding: 0;
      cursor: pointer;
      color: ${this.settings.color};
    }
    
    .axessibility__Item{
      display: flex;
      align-items: center;
      justify-content: space-between;
    }
    
    .axessibility__Item h3{
      color: ${this.settings.color};
      margin: 0;
      padding: 0;
      font-size: 16px;
      line-height: 1.06;
      margin-right: 24px;
      max-width: 115px;
      text-transform: uppercase;
    }
    
    .axessibility__Item div{
      display: flex;
      align-items: center;
      flex-wrap: wrap;
      justify-content: flex-end;
    }
    
    .axessibility__Item button{
      position: relative;
      cursor: pointer;
      min-width: 52px;
      max-width: 52px;
      min-height: 52px;
      max-height: 52px;
      line-height: 1;  
      border-radius: 50%;
      outline: 0;
      font-weight: bold;
      font-size: 24px;
      border: 1px solid ${this.settings.color};
      transition: background-color 300ms, color 300ms;
      color: ${this.settings.color} !important;
      background: transparent !important;
      margin: 17px 13px;
    }
    
    .axessibility__Item svg{
      position: absolute;
      top: 50%;
      left: 50%;
      visibility: visible !important;
      transform: translate(-50%, -50%);
    }
    
    .axessibility__Item button.isActive,
    .axessibility__Item button:hover{
      color: ${this.settings.background} !important;
      background-color: ${this.settings.color} !important;
    }
    
    .axessibility__ReadingLine{
      position: fixed;
      top: 0;
      left: 0;
      right: 0;
      z-index: 10000000;
      pointer-events: none;
      border-top: 2px solid yellow;
      border-bottom: 2px solid blue;
    }

    .axessibility__LargeCursor{
      position: fixed;
      top: 0;
      left: 0;
      z-index: 10000000;
      pointer-events: none;
    }
    
    @media screen and (max-width: 768px){
      .axessibility.top-left,
      .axessibility.top-right,
      .axessibility__Toggle.top-left,
      .axessibility__Toggle.top-right{
        top: 15px;
        bottom: auto;
      }
      
      .axessibility.top-right,
      .axessibility__Toggle.top-right{
        right: 15px;
      }
      
      .axessibility.top-left,
      .axessibility__Toggle.top-left{
        left: 15px;
      }
      
      .axessibility.bottom-left,
      .axessibility.bottom-right,
      .axessibility__Toggle.bottom-left,
      .axessibility__Toggle.bottom-right{
        top: auto;
        bottom: 15px;
      }
      
      .axessibility.bottom-right,
      .axessibility__Toggle.bottom-right{
        right: 15px;
      }
      
      .axessibility.bottom-left,
      .axessibility__Toggle.bottom-left{
        left: 15px;
      }
    }
    
    @media screen and (max-width: 425px){
      .axessibility{
        left: 15px;
        right: 15px;
        max-height: 100%;
        overflow-y: auto;
        padding: 33px 24px;
      }
      
      .axessibility__Item{
        flex-direction: column; 
        align-items: flex-start;
      }
      
      .axessibility__Item + .axessibility__Item{
        margin-top: 20px;
      }

      .axessibility__Item div{
        justify-content: flex-start;
      }
      
      .axessibility__Item h3{
        font-size: 14px;
        margin-bottom: 10px;
      }
      
      .axessibility__Item button{
        margin: 5px 17px 5px 0;
      }
    }
    `
  }
}

export default Axessibility;